pub trait VMCell {
    /// Perform a wrapped increment of the cell
    fn increment(&self) -> Self;
    /// Perform a wrapped decrement of the cell
    fn decrement(&self) -> Self;
}

impl VMCell for u8 {
    fn increment(&self) -> Self {
        self.wrapping_add(1)
    }
    fn decrement(&self) -> Self {
        self.wrapping_sub(1)
    }
}
