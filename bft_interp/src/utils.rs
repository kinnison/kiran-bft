use crate::VirtualMachine;
use bft_types::InstructionData;

impl<'a, T> VirtualMachine<'a, T> {
    /// Get the last instruction being interpreted
    /// ```
    /// use bft_types::{Program, InstructionData, RawInstruction};
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let first_instr = InstructionData::new(RawInstruction::DecrementPointer, (1,5));
    /// let my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.program_head(), first_instr);
    /// ```
    pub fn program_head(&self) -> InstructionData {
        self.program.instructions()[self.program_head]
    }
}
