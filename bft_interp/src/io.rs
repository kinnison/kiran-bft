use crate::VMError;
use crate::VMResult;
use crate::VirtualMachine;
use std::io::{Cursor, Read, Write};

impl<'a, T> VirtualMachine<'a, T>
where
    T: Clone + TryFrom<u8> + TryInto<u8> + ToString,
{
    /// Read from `reader` into cell at `self.tape[self.tape_head]`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// let mut readable_obj = std::io::Cursor::new(vec![2u8, 3u8]);
    ///
    /// assert_eq!(my_vm.read_to_cell(&mut readable_obj).unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), 2u8);
    /// ```
    pub fn read_to_cell(&mut self, reader: &mut impl Read) -> VMResult {
        let mut buf = vec![u8::MAX];
        reader.read_exact(buf.as_mut_slice())?;
        match T::try_from(buf[0]) {
            Ok(value) => {
                self.tape[self.tape_head] = value;
                Ok(self.program_head + 1)
            }
            Err(_) => Err(VMError::From {
                value: buf[0].to_string(),
            }),
        }
    }

    /// Write from cell at `self.tape[self.tape_head]` into writer.
    /// If OK, returns the next instruction pointer to use.
    /// It's difficult to write a doc test for this without making the whole
    /// io module public.
    pub fn write_from_cell(&self, writer: &mut impl MiniWrite) -> VMResult {
        match T::try_into(self.tape_head()) {
            Ok(head_u8) => {
                writer.write_all(vec![head_u8].as_slice())?;
                Ok(self.program_head + 1)
            }
            Err(_) => Err(VMError::Into {
                value: self.tape_head().to_string(),
            }),
        }
    }
}

/// Used as a man-in-the-middle output source.
/// We use this to check if the final output corresponds to a new line.
/// If not then add one!
pub struct ProxyWriter<T> {
    output: T,
    internal: Cursor<Vec<u8>>,
}

impl<T: Write> ProxyWriter<T> {
    /// Used to build new proxy writers.
    pub fn new(writer: T) -> Self {
        Self {
            output: writer,
            internal: Cursor::new(Vec::<u8>::new()),
        }
    }
}

pub trait MiniWrite {
    /// Same signature as [std::io::Write::write_all]
    fn write_all(&mut self, buf: &[u8]) -> std::io::Result<()>;
}

impl<T: Write> MiniWrite for ProxyWriter<T> {
    fn write_all(&mut self, buf: &[u8]) -> std::io::Result<()> {
        self.internal.write_all(buf)?;
        self.output.write_all(buf)
    }
}

impl<T> Drop for ProxyWriter<T> {
    fn drop(&mut self) {
        if let Some(byte) = self.internal.get_ref().last() {
            if *byte != 10u8 {
                println!()
            }
        }
    }
}
