use bft_types::RawInstruction;
use thiserror::Error;

pub type VMResult = Result<usize, VMError>;

#[derive(Error, Debug)]
pub enum VMError {
    /// The brainfuck program tried to move the tape head out of bounds.
    #[error(
        "[{file}:{line}:{column}] '{raw_instruction}' moves \
        tape head to invalid position {position} (must be within 0-{end})"
    )]
    InvalidHead {
        file: String,
        line: usize,
        column: usize,
        raw_instruction: RawInstruction,
        position: usize,
        end: usize,
    },
    /// Failed to read input.
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error("Failed to convert {value} (u8) to match tape typing")]
    From { value: String },
    #[error("Failed to convert {value} (tape typing) to u8")]
    Into { value: String },
    #[error(transparent)]
    BFError(#[from] bft_types::BFError),
    #[error("[{file}:{line}:{column}] VM lost track of brackets")]
    Brackets {
        line: usize,
        column: usize,
        file: String,
    },
}
