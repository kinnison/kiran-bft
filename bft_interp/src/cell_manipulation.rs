use crate::cell_trait::VMCell;
use crate::VMResult;
use crate::VirtualMachine;

impl<'a, T> VirtualMachine<'a, T>
where
    T: VMCell + Clone,
{
    /// Increment the value at the tape head.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.increment_cell().unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), 1u8);
    /// ```
    pub fn increment_cell(&mut self) -> VMResult {
        self.tape[self.tape_head] = self.tape_head().increment();
        Ok(self.program_head + 1)
    }

    /// Decrement the value at the tape head.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.decrement_cell().unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), u8::MAX);
    /// ```    
    pub fn decrement_cell(&mut self) -> VMResult {
        self.tape[self.tape_head] = self.tape_head().decrement();
        Ok(self.program_head + 1)
    }
}
