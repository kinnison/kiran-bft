use crate::VMError;
use crate::VMResult;
use crate::VirtualMachine;

impl<'a, T: Clone> VirtualMachine<'a, T> {
    /// Clone the cell at the tape head
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// my_vm.increment_cell();
    /// my_vm.increment_cell();
    ///
    /// assert_eq!(my_vm.tape_head(), 2u8);
    /// ```
    pub fn tape_head(&self) -> T {
        self.tape[self.tape_head].clone()
    }
}

impl<'a, T: Default> VirtualMachine<'a, T> {
    /// Check that the tape head is in bounds
    fn check_head(&mut self) -> VMResult {
        if !(0..self.tape.len()).contains(&self.tape_head) {
            if !self.can_grow {
                let line = self.program_head().line();
                let column = self.program_head().line();
                let file = self.program.file_name().display().to_string();
                let position = self.tape_head;
                let end = self.tape.len();
                return Err(VMError::InvalidHead {
                    file,
                    line,
                    column,
                    raw_instruction: *self.program_head().raw_instruction(),
                    position,
                    end,
                });
            } else {
                self.tape.push(Default::default());
            };
        };
        Ok(self.program_head)
    }

    /// Move tape head backwards, i.e. decrement `self.tape_head`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// my_vm.tape_head_forwards();
    ///
    /// assert_eq!(my_vm.tape_head_backwards().unwrap(), 1);
    /// assert!(my_vm.tape_head_backwards().is_err());
    /// ```
    pub fn tape_head_backwards(&mut self) -> VMResult {
        self.tape_head = self.tape_head.checked_sub(1).ok_or(VMError::InvalidHead {
            line: self.program_head().line(),
            column: self.program_head().column(),
            raw_instruction: *self.program_head().raw_instruction(),
            file: self.program.file_name().display().to_string(),
            position: self.tape_head,
            end: self.tape.len(),
        })?;
        Ok(self.program_head + 1)
    }

    /// Move tape head forwards i.e. increment `self.tape_head`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.tape_head_forwards().unwrap(), 1);
    /// ```
    pub fn tape_head_forwards(&mut self) -> VMResult {
        self.tape_head += 1;
        self.check_head()?;
        Ok(self.program_head + 1)
    }
}

#[cfg(test)]
mod tests {
    use crate::{Program, VirtualMachine};

    fn get_good_program() -> Program {
        let test_contents = "
            > <+CHOO-.
            ,[CHOO]
        ";
        let test_name = "test.bf";
        Program::new(test_contents, test_name)
    }

    #[test]
    fn test_check_head() {
        let good_program = get_good_program();

        let mut fixed_tape = <VirtualMachine>::new(&good_program, 1, false);
        assert!(fixed_tape.check_head().is_ok());
        assert!(fixed_tape.tape_head_forwards().is_err());
        assert!(fixed_tape.check_head().is_err());

        let mut fixed_tape = <VirtualMachine>::new(&good_program, 1, true);
        assert!(fixed_tape.check_head().is_ok());
        assert!(fixed_tape.tape_head_forwards().is_ok());
    }
}
