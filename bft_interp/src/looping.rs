use crate::VMError;
use crate::VMResult;
use crate::VirtualMachine;
use bimap::BiMap;
use core::panic;
use std::cmp::PartialEq;

impl<'a, T> VirtualMachine<'a, T>
where
    T: Clone + TryFrom<u8> + PartialEq,
{
    /// If cell at tape head is nonzero then find the instruction after the
    /// corresponding open bracket. Else, point to the next instruction after
    /// the current.
    /// Correspondence is determined by `open_to_closed`.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// use bimap::BiMap;
    /// let dummy = Program::new(".", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&dummy, 0, false);
    ///
    /// let mut open_to_closed = BiMap::new();
    /// open_to_closed.insert(2,0);
    ///
    /// assert_eq!(my_vm.loop_end(&open_to_closed).unwrap(), 1);
    ///
    /// my_vm.increment_cell();
    /// assert_eq!(my_vm.loop_end(&open_to_closed).unwrap(), 3);
    /// ```
    pub fn loop_end(&self, open_to_closed: &BiMap<usize, usize>) -> VMResult {
        let cell_zero = match T::try_from(0u8) {
            Ok(number) => number,
            _ => panic!("Cell type has no zero value"),
        };
        // let next_instruction = match self.tape_head() {
        //     cell_zero => *open_to_closed.get(&self.program_head).unwrap(),
        //     _ => self.program_head + 1,
        // };
        let next_instruction = if self.tape_head() != cell_zero {
            *open_to_closed
                .get_by_right(&self.program_head)
                .ok_or(VMError::Brackets {
                    line: self.program_head().line(),
                    column: self.program_head().column(),
                    file: self.program.file_name().to_string_lossy().to_string(),
                })?
                + 1
        } else {
            self.program_head + 1
        };
        Ok(next_instruction)
    }

    /// Point to the corresponding closed bracket, no matter the cell value at
    /// the tape head.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// use bimap::BiMap;
    /// let invalid_program = Program::new("]-[-", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&invalid_program, 0, false);
    ///
    /// let mut open_to_closed = BiMap::new();
    /// open_to_closed.insert(0,2);
    ///
    /// assert_eq!(my_vm.loop_start(&open_to_closed).unwrap(), 2);
    ///
    /// my_vm.increment_cell();
    /// assert_eq!(my_vm.loop_start(&open_to_closed).unwrap(), 2);
    /// ```
    pub fn loop_start(&self, open_to_closed: &BiMap<usize, usize>) -> VMResult {
        let next_instruction =
            *open_to_closed
                .get_by_left(&self.program_head)
                .ok_or(VMError::Brackets {
                    line: self.program_head().line(),
                    column: self.program_head().column(),
                    file: self.program.file_name().to_string_lossy().to_string(),
                })?;
        Ok(next_instruction)
    }
}
