use std::io::{Read, Write};

use crate::cell_trait::VMCell;
use crate::errors::VMResult;
use crate::io::ProxyWriter;
use crate::VirtualMachine;

impl<'a, T> VirtualMachine<'a, T>
where
    T: VMCell + Default + Clone + TryFrom<u8> + TryInto<u8> + ToString + std::cmp::PartialEq,
{
    /// Interpret the program.
    /// Output is written to `output` and input is taken from `input`.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// use std::io::Cursor;
    /// let my_program = Program::new("++>+++++[<+>-]>.<.<.", "some_file.bf");
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// let mut input = Cursor::new(Vec::<u8>::new());
    /// let mut output = Cursor::new(Vec::<u8>::new());
    /// my_vm.interpret(&mut input, &mut output);
    ///
    /// assert_eq!(output.get_ref(), &[0,0,7]);
    /// ```
    pub fn interpret(&mut self, mut input: &mut impl Read, output: &mut impl Write) -> VMResult {
        let open_to_closed = self.program.bracket_analysis()?;
        let mut proxy_writer = ProxyWriter::new(output);
        while self.program_head < self.program.instructions().len() {
            self.program_head = match self.program_head().raw_instruction() {
                bft_types::RawInstruction::IncrementPointer => self.tape_head_forwards()?,
                bft_types::RawInstruction::DecrementPointer => self.tape_head_backwards()?,
                bft_types::RawInstruction::IncrementByte => self.increment_cell()?,
                bft_types::RawInstruction::DecrementByte => self.decrement_cell()?,
                bft_types::RawInstruction::PrintByte => self.write_from_cell(&mut proxy_writer)?,
                bft_types::RawInstruction::StoreBtye => self.read_to_cell(&mut input)?,
                bft_types::RawInstruction::LoopStart => self.loop_start(&open_to_closed)?,
                bft_types::RawInstruction::LoopEnd => self.loop_end(&open_to_closed)?,
            };
        }
        Ok(0usize)
    }
}
