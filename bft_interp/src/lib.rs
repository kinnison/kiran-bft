/*!
This library provides the [VirtualMachine] struct that can be used for interpreting
brainfuck code.
 */

use bft_types::Program;
use std::fmt::Debug;

mod cell_manipulation;
mod cell_trait;
mod errors;
mod interpret;
mod io;
mod looping;
mod tape_navigation;
mod utils;

use errors::{VMError, VMResult};

#[derive(Debug)]
pub struct VirtualMachine<'a, T = u8> {
    /// The bf program to be interpreted
    program: &'a Program,
    /// Virtual machine's tape
    tape: Vec<T>,
    /// Current position position being pointed to in the tape
    tape_head: usize,
    /// Current position being interpreted in the bf program
    program_head: usize,
    /// Whether or not the virtual machine tape can grow
    can_grow: bool,
}

impl<'a, T> VirtualMachine<'a, T>
where
    T: Default + Clone,
{
    /// Used to build a new virtual machine.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf");
    /// let my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// ```
    pub fn new(program: &'a Program, len: usize, can_grow: bool) -> Self {
        Self {
            program,
            tape: vec![Default::default(); if len == 0 { 3000 } else { len }],
            tape_head: 0,
            program_head: 0,
            can_grow,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Program, VirtualMachine};

    fn get_good_program() -> Program {
        let test_contents = "
            > <+CHOO-.
            ,[CHOO]
        ";
        let test_name = "test.bf";
        Program::new(test_contents, test_name)
    }

    #[test]
    fn test_constructor() {
        let good_program = get_good_program();
        let virtual_machine = <VirtualMachine>::new(&good_program, 0, false);
        assert_eq!(virtual_machine.tape.len(), 3000);
        assert_eq!(virtual_machine.tape_head, 0);
    }
}
