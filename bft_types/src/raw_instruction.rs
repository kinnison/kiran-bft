use std::fmt;
use variant_count::VariantCount;

/// Raw brainfuck instructions
#[derive(Debug, VariantCount, PartialEq, Eq, Clone, Copy)]
pub enum RawInstruction {
    /// Corresponds to the `>` character
    IncrementPointer,
    /// Corresponds to the `<` character
    DecrementPointer,
    /// Corresponds to the `+` character
    IncrementByte,
    /// Corresponds to the `-` character
    DecrementByte,
    /// Corresponds to the `.` character
    PrintByte,
    /// Corresponds to the `,` character
    StoreBtye,
    /// Corresponds to the `[` character
    LoopStart,
    /// Corresponds to the `]` character
    LoopEnd,
}

impl RawInstruction {
    /// Convert a character into the corresponding [`RawInstruction`]
    pub fn from_char(c: char) -> Option<RawInstruction> {
        match c {
            '>' => Some(RawInstruction::IncrementPointer),
            '<' => Some(RawInstruction::DecrementPointer),
            '+' => Some(RawInstruction::IncrementByte),
            '-' => Some(RawInstruction::DecrementByte),
            '.' => Some(RawInstruction::PrintByte),
            ',' => Some(RawInstruction::StoreBtye),
            '[' => Some(RawInstruction::LoopStart),
            ']' => Some(RawInstruction::LoopEnd),
            _ => None,
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RawInstruction::IncrementPointer => write!(f, ">"),
            RawInstruction::DecrementPointer => write!(f, "<"),
            RawInstruction::IncrementByte => write!(f, "+"),
            RawInstruction::DecrementByte => write!(f, "-"),
            RawInstruction::PrintByte => write!(f, "."),
            RawInstruction::StoreBtye => write!(f, ","),
            RawInstruction::LoopStart => write!(f, "["),
            RawInstruction::LoopEnd => write!(f, "]"),
        }
    }
}

/// Get a description of the effect of the [`RawInstruction`]
impl From<&RawInstruction> for String {
    fn from(raw_instruction: &RawInstruction) -> Self {
        let description = match raw_instruction {
            RawInstruction::IncrementPointer => "Increment the data pointer",
            RawInstruction::DecrementPointer => "Decrement the data pointer",
            RawInstruction::IncrementByte => "Increment the byte at the data pointer.",
            RawInstruction::DecrementByte => "Decrement the byte at the data pointer.",
            RawInstruction::PrintByte => "Output the byte at the data pointer.",
            RawInstruction::StoreBtye => "Store to byte at the data pointer.",
            RawInstruction::LoopStart => "Start looping.",
            RawInstruction::LoopEnd => "End looping.",
        };
        description.to_string()
    }
}

#[cfg(test)]
mod tests {
    use crate::RawInstruction;

    #[test]
    /// Check that there are 8 variants of [`RawInstruction`]
    fn eight_instruction_variants() {
        assert_eq!(RawInstruction::VARIANT_COUNT, 8);
    }
}
