use bimap::BiMap;
use line_col::LineColLookup;
use std::path::{Path, PathBuf};
use std::{fmt, fs};

use crate::{BFError, InstructionData, RawInstruction};

/// A brainfuck program
#[derive(Debug)]
pub struct Program {
    /// The raw instructions
    instructions: Vec<InstructionData>,
    /// The file where the instructions were found
    file_name: PathBuf,
}

impl Program {
    /// Create a [`Program`] object from a file's contents and its name.
    /// The [`LineColLookup`] crate is used to index each character with
    /// its associated line and column number.
    ///
    /// For example, if the file `test.bf` contained `[.]` then we could
    /// load this program by calling:
    /// ```
    /// # use bft_types::{RawInstruction, InstructionData, Program};
    /// let program = Program::new("[.]", "test.bf");
    /// let first_instruction = &program.instructions()[0];
    /// matches!(first_instruction.raw_instruction(), RawInstruction::LoopStart);
    /// assert_eq!(first_instruction.line(), 1);
    /// assert_eq!(first_instruction.column(), 1);
    /// ```
    pub fn new<P>(file_contents: &str, file_name: P) -> Self
    where
        P: AsRef<Path>,
    {
        let lookup = LineColLookup::new(file_contents);
        let instructions: Vec<InstructionData> = file_contents
            .char_indices()
            .filter_map(|(n, c)| {
                RawInstruction::from_char(c)
                    .map(|raw_instruction| InstructionData::new(raw_instruction, lookup.get(n)))
            })
            .collect();
        Self {
            file_name: file_name.as_ref().to_path_buf(),
            instructions,
        }
    }

    /// Create a [`Program`] object from a path to a file.
    ///
    /// For example, to load the brainfuck program contained in
    /// `/some/dir/test.bf` then run:
    /// ```
    /// # use bft_types::Program;
    /// let program = Program::from_file("/some/dir/test.bf");
    /// ```
    pub fn from_file<P>(file_name: P) -> Result<Program, BFError>
    where
        P: AsRef<Path>,
    {
        let file_contents = fs::read_to_string(&file_name)?;
        let program = Program::new(&file_contents, &file_name);
        Ok(program)
    }

    /// Checks that square brackets within the program are balanced.
    /// This function builds a stack (called `brackets`) and iterates through the program.
    /// Each `[` instruction that it finds it adds to `brackets`.
    /// For each `]` it finds it removes the last `[` from `brackets`.
    /// If it is unable to do so or if `brackets` it still populated at the end then the brackets
    /// are assumed to be unbalanced.
    pub fn bracket_analysis(&self) -> Result<BiMap<usize, usize>, BFError> {
        let mut bracket_stack = Vec::<(usize, &InstructionData)>::new();
        let mut matched_bracket_positions = BiMap::<usize, usize>::new();
        for (position, instruction) in self.instructions.iter().enumerate() {
            let raw_instruction = instruction.raw_instruction();
            if raw_instruction == &RawInstruction::LoopStart {
                bracket_stack.push((position, instruction));
            } else if raw_instruction == &RawInstruction::LoopEnd {
                if let Some(last_bracket) = bracket_stack.pop() {
                    matched_bracket_positions.insert(last_bracket.0, position);
                } else {
                    return Err(BFError::UnmatchedBracket {
                        bracket: ']',
                        line: instruction.line(),
                        column: instruction.column(),
                    });
                }
            }
        }
        if let Some((_, instr)) = bracket_stack.last() {
            return Err(BFError::UnmatchedBracket {
                bracket: '[',
                line: instr.line(),
                column: instr.column(),
            });
        }
        Ok(matched_bracket_positions)
    }

    /// Get the instructions found in the program.
    pub fn instructions(&self) -> &[InstructionData] {
        &self.instructions
    }

    /// Get the name of the file from where the program was loaded.
    pub fn file_name(&self) -> &Path {
        &self.file_name
    }
}

impl fmt::Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let file_name = match self.file_name().file_name() {
            Some(file) => file.to_string_lossy(),
            None => self.file_name().to_string_lossy(),
        };
        let program_description: Vec<String> = self
            .instructions()
            .iter()
            .map(|x| {
                let description = String::from(x.raw_instruction());
                let line = x.line();
                let column = x.column();
                format!("[{file_name}:{line}:{column}] {description}")
            })
            .collect();
        write!(f, "{}", program_description.join("\n"))
    }
}

#[cfg(test)]
mod tests {
    use crate::Program;
    use crate::RawInstruction;

    fn get_good_program() -> Program {
        let test_contents = "
            > <+CHOO-.
            ,[CHOO]
        ";
        let test_name = "test.bf";
        Program::new(test_contents, test_name)
    }

    fn get_bad_program() -> Program {
        let test_contents = "
            > <+CHOO-.
            ,[CHOO]]
        ";
        let test_name = "test.bf";
        Program::new(test_contents, test_name)
    }

    #[test]
    /// Check that [`Program::new`] correctly identifies brainfuck code
    fn bf_correctly_parsed() {
        let test_program = get_good_program();
        let instructions = test_program.instructions();

        assert_eq!(instructions.len(), 8);

        assert_eq!(
            instructions[0].raw_instruction(),
            &RawInstruction::IncrementPointer
        );
        assert_eq!(
            instructions[1].raw_instruction(),
            &RawInstruction::DecrementPointer
        );
        assert_eq!(
            instructions[2].raw_instruction(),
            &RawInstruction::IncrementByte
        );
        assert_eq!(
            instructions[3].raw_instruction(),
            &RawInstruction::DecrementByte
        );
        assert_eq!(
            instructions[4].raw_instruction(),
            &RawInstruction::PrintByte
        );
        assert_eq!(
            instructions[5].raw_instruction(),
            &RawInstruction::StoreBtye
        );
        assert_eq!(
            instructions[6].raw_instruction(),
            &RawInstruction::LoopStart
        );
        assert_eq!(instructions[7].raw_instruction(), &RawInstruction::LoopEnd);
    }

    #[test]
    /// Check that [`Program::new`] correctly identifies lines
    fn lines_correctly_identified() {
        let test_program = get_good_program();
        let instructions = test_program.instructions();
        assert_eq!(instructions[0].line(), 2);
        assert_eq!(instructions[2].line(), 2);
        assert_eq!(instructions[2].line(), 2);
        assert_eq!(instructions[3].line(), 2);
        assert_eq!(instructions[4].line(), 2);
        assert_eq!(instructions[5].line(), 3);
        assert_eq!(instructions[6].line(), 3);
        assert_eq!(instructions[7].line(), 3);
    }

    #[test]
    /// Check that [`Program::new`] correctly identifies column
    fn columns_correctly_identified() {
        let test_program = get_good_program();
        let instructions = test_program.instructions();

        assert_eq!(instructions[0].column(), 13);
        assert_eq!(instructions[1].column(), 15);
        assert_eq!(instructions[2].column(), 16);
        assert_eq!(instructions[3].column(), 21);
        assert_eq!(instructions[4].column(), 22);
        assert_eq!(instructions[5].column(), 13);
        assert_eq!(instructions[6].column(), 14);
        assert_eq!(instructions[7].column(), 19);
    }

    #[test]
    /// Check that [`Program::check_brackets_balanced`] correctly identifies
    /// balanced brackets
    fn balanced_brackets_identified() {
        let test_program = get_good_program();
        assert!(test_program.bracket_analysis().is_ok());
    }

    #[test]
    /// Check that [`Program::check_brackets_balanced`] correctly identifies
    /// unbalanced brackets
    fn unbalanced_brackets_identified() {
        let test_program = get_bad_program();
        assert!(test_program.bracket_analysis().is_err());
    }
}
