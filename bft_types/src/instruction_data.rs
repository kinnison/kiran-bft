use crate::RawInstruction;

/// Store a brainfuck instruction
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct InstructionData {
    /// The instruction itself
    raw_instruction: RawInstruction,
    /// The line and column where the instruction was found
    line_col: (usize, usize),
}

impl InstructionData {
    /// Create an [`InstructionData`] object
    pub fn new(raw_instruction: RawInstruction, line_col: (usize, usize)) -> Self {
        Self {
            raw_instruction,
            line_col,
        }
    }
    /// Get the raw instruction
    pub fn raw_instruction(&self) -> &RawInstruction {
        &self.raw_instruction
    }
    /// Get the line number where the instruction was found
    pub fn line(&self) -> usize {
        self.line_col.0
    }
    /// Get the column number where the instruction was found
    pub fn column(&self) -> usize {
        self.line_col.1
    }
}
