/*!
This library provides types that are useful for interpreting brainfuck code.
 */
use thiserror::Error;

#[derive(Error, Debug)]
pub enum BFError {
    /// The brainfuck program contains an unmatched square bracket
    #[error("unmatched {bracket} on line {line} column {column}")]
    UnmatchedBracket {
        bracket: char,
        line: usize,
        column: usize,
    },
    /// Failed to open file containing brainfuck program
    #[error(transparent)]
    IOError(#[from] std::io::Error),
}
