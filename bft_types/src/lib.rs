/*!
This library provides types that are useful for interpreting brainfuck code.
 */
mod errors;
mod instruction_data;
mod program;
mod raw_instruction;

pub use errors::BFError;
pub use instruction_data::InstructionData;
pub use program::Program;
pub use raw_instruction::RawInstruction;
