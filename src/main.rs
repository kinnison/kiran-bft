/*!
A command-line tool for interpreting brainfuck code.
Uses the [bft_types] and [bft_interp] libraries.
*/

use std::io::{stdin, stdout};

use anyhow::Result;
use bft_interp::VirtualMachine;
use bft_types::Program;
use clap::Parser;
use cli::Options;

mod cli;

/// Uses the info stored in `options` to run the brainfuck interpreter.
fn run_bft(options: &Options) -> Result<()> {
    let program = Program::from_file(&options.program)?;
    let mut virtual_machine =
        <VirtualMachine>::new(&program, options.cells.try_into()?, options.extensible);
    virtual_machine.interpret(&mut stdin(), &mut stdout())?;
    Ok(())
}

/// Main program for bft
fn main() {
    let options = cli::Options::parse();
    if let Err(err) = run_bft(&options) {
        eprintln!("{err}");
        std::process::exit(1);
    };
}
